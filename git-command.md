# Perintah Git Sehari-hari #

1. Membuat repository di local

    ```
    cd c:/Users/endy/folder/project
    git init
    ```

2. Menambah file ke staging area

    ```
    git add namafile.txt
    git add namafolder
    ```

3. Menyimpan perubahan di staging area ke local repo

    ```
    git commit -m "keterangan perubahan"
    ```

4. Mendaftarkan remote repository

    ```
    git remote add gitlab git@gitlab.com:training-devops-201901/aplikasi-customer.git
    ```

5. Upload isi local repo ke remote repo

    ```
    git push namaremote namabranch
    ```

    misalnya

    ```
    git push gitlab master
    ```

[![Git Skema](img/03-git-commit.jpg)](img/03-git-commit.jpg)