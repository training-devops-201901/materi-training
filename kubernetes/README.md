# Kubernetes #

[![Konsep Kubernetes](../img/konsep-kubernetes.jpg)](../img/konsep-kubernetes.jpg)

## Daftar Perintah Kubernetes ##

1. Load config file ke environment variable

    Windows

        set KUBECONFIG=k8s-training-devops-201901-kubeconfig.yaml
    
    Linux

        export KUBECONFIG=k8s-training-devops-201901-kubeconfig.yaml

2. Melihat daftar node dalam cluster

        kubectl get nodes

3. Melihat daftar object dalam cluster

        kubectl get pod,svc,deployments,endpoints

4. Menjalankan file manifest

        kubectl create -f 01-persistent-volume-claim.yml

5. Menampilkan logs aplikasi

        kubectl logs -f <nama-deployment>
    
    contoh :

        kubectl logs -f pod/aplikasi-customer-app-795c69867-ckbjr

6. Menghapus object

        kubectl delete pod/aplikasi-customer-app-795c69867-ckbjr

7. Scale / Replikasi

        kubectl scale --replicas=5 deployment.extensions/aplikasi-customer-app

    Hasilnya seperti ini :

        $ kubectl get deployment,service,pods
        NAME                                          READY   UP-TO-DATE   AVAILABLE   AGE
        deployment.extensions/aplikasi-customer-app   2/5     5            2           12m
        deployment.extensions/customer-db             1/1     1            1           8m23s

        NAME                          TYPE           CLUSTER-IP       EXTERNAL-IP       PORT(S)        AGE
        service/customer-db-service   ClusterIP      10.245.111.114   <none>            3306/TCP       8m24s
        service/customer-lb           LoadBalancer   10.245.188.121   165.227.243.134   80:30552/TCP   81s
        service/kubernetes            ClusterIP      10.245.0.1       <none>            443/TCP        13m

        NAME                                        READY   STATUS              RESTARTS   AGE
        pod/aplikasi-customer-app-795c69867-ckbjr   1/1     Running             4          12m
        pod/aplikasi-customer-app-795c69867-jsm5b   0/1     ContainerCreating   0          5s
        pod/aplikasi-customer-app-795c69867-l4rc7   0/1     ContainerCreating   0          5s
        pod/aplikasi-customer-app-795c69867-md5sp   0/1     ContainerCreating   0          5s
        pod/aplikasi-customer-app-795c69867-z9nhv   1/1     Running             1          12m
        pod/customer-db-6f98c94757-vwpz7            1/1     Running             0          8m23s

[![Skema Deployment](../img/k8s-deployment.jpg)](../img/k8s-deployment.jpg)